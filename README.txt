==================================================
Ocean Observatories Initiative Cyberinfrastructure
Integrated Observatory Network (ION)
ioncore-integration - Integration tests for OOICI
==================================================
 
April 2010 - September 2010 (C) UCSD Regents

This project provides a integration testing framework for all 
the services of the OOI release 1 system with their full 
architectural dependencies in Python.

For more information, please see:

Dependencies
============


Usage
=====


Testing
=======


Build and Packaging using Ant
=============================



Change log:
===========
.
